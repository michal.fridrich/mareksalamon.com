WPMF-Theme
********
Version: 1.0.0

## Getting started

1. Clone repo into directory
2. Run npm i
3. Map to remote in ftpcred.js (change ftpcred-default.js to ftpcred.js)
4. Run gulp (--development, --stage, --production)
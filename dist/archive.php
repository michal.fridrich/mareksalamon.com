<section id="works-list">
    <div class="container-fluid position-relative works-wrapper p-3 p-sm-4 p-md-6 g-5 mb-5 mb-lg-8">
        <div class="row works-filter py-5">
            <div class="col-12">
                <ul id="filters" class="">
                    <li><a href="#" data-filter="*" class="selected me-3">Vše</a></li>
                    <?php
                    $terms = get_terms("category");
                    $count = count($terms);
                    if ( $count > 0 ){
                        foreach ( $terms as $term ) {  //for each term:
                            echo "<li class='py-3 me-3 ".$term->slug."'><a href='#' data-filter='.".$term->slug."'>" . $term->name . "</a></li>\n";
                        }
                    }
                    ?>
                </ul>
            </div> <!-- /.com-12 -->
        </div>
        <div id="isotope-list" class="row works-list g-5">
            <?php
            $args = array(
                'posts_per_page' => -1,
                'post_type' => 'post',
                'orderby' => 'date',
                'order' => 'DESC',
            );
            query_posts($args);
            while ( have_posts() ) : the_post();

                $termsArray = get_the_terms( $post->ID, "category" );
                $termsString = "";
                foreach ( $termsArray as $term ) {
                    $termsString .= $term->slug.' ';
                }

                $post_slug = get_post_field( 'post_name', get_post() );
                $categories = get_the_category();

                if ( get_field( 'work_vimeo' ) ):
                    $video_link = get_field('work_vimeo');
                    $video_link_key = str_replace("https://vimeo.com/", "", $video_link);
                    $post_type_class = " works-vimeo";
                elseif ( get_field( 'work_youtube' ) ):
                    $video_link = get_field('work_youtube');
                    $video_link_key = str_replace("https://www.youtube.com/watch?v=", "", $video_link);
                    $post_type_class = " works-youtube";
                    if (!has_post_thumbnail( $post->ID ) ):
                        $featured_img_url = "https://img.youtube.com/vi/" . $video_link_key . "/maxresdefault.jpg";
                    endif;
                elseif ( get_field( 'work_soundcloud' ) ):
                    $sound_link = get_field('work_soundcloud');
                    $post_type_class = " works-soundcloud";
                else :
                    $post_link = get_permalink();
                    $post_type_class = " works-content";
                endif;

                $credits = pods_field_display('work_credits_cz');
                $description = pods_field_display('work_desc_cz');

                if (has_post_thumbnail( $post->ID ) ):
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'medium');
                endif;
                ?>

                <!-- Card: Works - Item -->
                <div class="<?php echo $termsString; ?>item isotope-item col-12 col-md-6 col-lg-4 col-xxxl-3 <?php if ( ! empty( $categories ) ) { echo esc_html( $categories[0]->slug );};?>">
                    <div
                    class="works-item ratio ratio-16x9 works-item--wrapper<?php echo $post_type_class; ?>" style="background-image: url('<?php echo $featured_img_url; ?>');"
                    data-bs-toggle="offcanvas"
                    data-bs-target="#offcanvas-work-<?php echo $post_slug; ?>"
                    aria-controls="offcanvas-work-<?php echo $post_slug; ?>"
                    <?php if ( get_field( 'work_vimeo' ) ): ?>
                    data-video="https://player.vimeo.com/video/<?php echo $video_link_key; ?>"
                    <?php elseif ( get_field( 'work_youtube' ) ): ?>
                    data-video="https://www.youtube.com/embed/<?php echo $video_link_key; ?>"
                    <?php endif; ?>
                    data-slug="<?php echo $post_slug;?>"
                    data-thumbnail="<?php echo $featured_img_url; ?>">
                    <div class="works-item-overlay position-absolute top-0 start-0 h-100 w-100">
                    </div>
                        <div class="works-item--content">
                            <span class="badge bg-secondary"></span>
                            <h5 class="position-absolute bottom-0 start-0 px-4 pb-3 works-item--title"><?php the_title(); ?></h5>
                            <div class="works-item--categories top-0 start-0 px-4 pt-3 ">
                            <?php $categories = get_the_category();
                            foreach( $categories as $category) {
                                $name = $category->name;
                                $category_link = get_category_link( $category->term_id );
                                echo "<div class='badge bg-secondary me-2'>
                                        <span class=" . esc_attr( $category->slug) . ">" . esc_attr( $name) . "</span>
                                     </div>";
                            } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Card: Works - Item -->

                <!-- Offcanvas: Works - Item -->
                <div class="offcanvas offcanvas-end mt-0 w-100 bg-secondary px-0 px-md-3 px-lg-5" tabindex="-1" id="offcanvas-work-<?php echo $post_slug; ?>" aria-labelledby="title-<?php echo $post_slug; ?>">
                    <div class="offcanvas-bg position-absolute top-0 start-0 w-100 h-100" style="background-image: url('<?php echo $featured_img_url; ?>');">
                    </div>
                    <div class="offcanvas-header position-relative pe-0 py-md-5">
                        <h5 class="offcanvas-title" id="title-<?php echo $post_slug; ?>">
                            <?php the_title(); ?>
                        </h5>
                        <button type="button" class="btn btn-lg text-reset has-shadow offcanvas-close d-none d-md-block" data-bs-dismiss="offcanvas" aria-label="Zavřít" data-videoplaying="<?php echo $post_slug; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                                <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
                            </svg>
                        </button>
                    </div>
                    <div class="offcanvas-body position-relative pt-0 pb-5 px-0 px-md-3 ">
                        <div class="container-fluid h-100 px-0">
                            <div class="row h-100 mx-0">
                                <div class="col-12 col-lg-9 px-0 pe-lg-5 h-60 h-md-70 h-lg-100 mb-3 mb-lg-0">
                                    <?php if ( get_field( 'work_vimeo' ) ): ?>
                                        <div style="padding:56.25% 0 0 0;position:relative;"><iframe id="videoplayer-<?php echo $post_slug; ?>" src="https://player.vimeo.com/video/<?php $video_link_key; ?>?api=1&player_id=player" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
                                    <?php elseif ( get_field( 'work_youtube' ) ): ?>
                                        <iframe
                                            id="videoplayer-<?php echo $post_slug; ?>"
                                            src=""
                                            class="ratio ratio-16x9 h-100 videoplayer-youtube"
                                            frameborder="0"
                                            enablejsapi="1"
                                            allow="accelerometer; autoplay; encrypted-media; gyroscope;"
                                            allowfullscreen>
                                        </iframe>
                                    <?php elseif ( get_field( 'work_soundcloud' ) ): ?>
                                        <?php echo $sound_link; ?>
                                    <?php else : ?>
                                    <div class="content-notvideo">
                                        <?php the_content(); ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-12 col-lg-3 px-3 px-md-0">
                                    <div class="color-primary mb-5"><?php echo $description; ?></div>
                                    <div class="fs-smallest"><?php echo $credits; ?></div>
                                    <?php $categories = get_the_category();
                                    foreach( $categories as $category) {
                                        $name = $category->name;
                                        $category_link = get_category_link( $category->term_id );
                                        echo "<div class='badge bg-secondary me-2'>
                                                <span class=" . esc_attr( $category->slug) . ">" . esc_attr( $name) . "</span>
                                             </div>";
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="offcanvas-footer position-relative py-3 py-md-5 px-3">
                        <div class="container-fluid px-0">
                            <div class="row mx-0">
                                <?php
                                $next_post = get_next_post();
                                if ( is_a( $next_post , 'WP_Post' ) ) :
                                $next_post_slug = get_post_field( 'post_name', get_next_post() );
                                $next_post_video = get_post_meta( $next_post->ID, 'work_youtube', $single = true);
                                $next_post_video_key = str_replace("https://www.youtube.com/watch?v=", "", $next_post_video);
                                $next_post_video_embed = "https://www.youtube.com/embed/" . $next_post_video_key;
                                ?>
                                <div class="col text-start p-2">
                                    <div
                                    class="works-item-next"
                                    data-bs-toggle="offcanvas"
                                    data-bs-target="#offcanvas-work-<?php echo $next_post_slug; ?>"
                                    aria-controls="offcanvas-work-<?php echo $next_post_slug; ?>"
                                    data-video="<?php echo $next_post_video_embed; ?>"
                                    data-slug="<?php echo $next_post_slug; ?>"
                                    data-videoplaying="<?php echo $post_slug; ?>">
                                        <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd">
                                            <path d="M2.117 12l7.527 6.235-.644.765-9-7.521 9-7.479.645.764-7.529 6.236h21.884v1h-21.883z"/>
                                        </svg>
                                        <span class="d-none d-md-inline-block"><?php echo get_the_title( $next_post->ID ); ?></span>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <div class="col-auto text-center ps-0 d-md-none">
                                    <button type="button" class="btn p-2 text-reset has-shadow offcanvas-close" data-bs-dismiss="offcanvas" aria-label="Zavřít" data-videoplaying="<?php echo $post_slug; ?>">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                                            <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
                                        </svg>
                                    </button>
                                </div>
                                <?php
                                $prev_post = get_previous_post();
                                if ( is_a( $prev_post , 'WP_Post' ) ) :
                                $prev_post_slug = get_post_field( 'post_name', get_previous_post() );
                                $prev_post_video = get_post_meta( $prev_post->ID, 'work_youtube', $single = true);
                                $prev_post_video_key = str_replace("https://www.youtube.com/watch?v=", "", $prev_post_video);
                                $prev_post_video_embed = "https://www.youtube.com/embed/" . $prev_post_video_key;
                                ?>
                                <div class="col text-end pe-2">
                                    <div data-bs-toggle="offcanvas"
                                    class="works-item-prev ms-auto"
                                    data-bs-target="#offcanvas-work-<?php echo $prev_post_slug; ?>"
                                    aria-controls="offcanvas-work-<?php echo $prev_post_slug; ?>"
                                    data-video="<?php echo $prev_post_video_embed; ?>"
                                    data-slug="<?php echo $prev_post_slug; ?>"
                                    data-videoplaying="<?php echo $post_slug; ?>">
                                        <span class="d-none d-md-inline-block"><?php echo get_the_title( $prev_post->ID ); ?></span>
                                        <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M21.883 12l-7.527 6.235.644.765 9-7.521-9-7.479-.645.764 7.529 6.236h-21.884v1h21.883z"/></svg>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Offcanvas: Works - Item -->
            <?php
            endwhile;
            ?>
            </div>
        </div>
    </div>
</section>
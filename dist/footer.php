		</main><!-- /#main -->
		<hr>
		<footer id="footer" class="footer bg-transparent">
			<div class="container-fluid px-0">
	            <div class="row footer-primary mx-0 px-3 py-4 p-sm-4 p-md-6 g-5">
	                <div class="col-12 col-lg-4 px-0 pe-0 pe-md-5 pb-2 pb-md-3">
	                    <h3>
	                    	<?php esc_html_e( 'Těším se na Váš kontakt a spolupráci', 'wpmf-themedev-v1' ); ?>
	                    </h3>
	                </div>
        			<div class="col-12 col-md-6 col-lg-4 px-0 pe-lg-5 pb-2 pb-md-5">
        				<h4><?php esc_html_e( 'Napište', 'wpmf-themedev-v1' ); ?></h4>
						<a class="ps-0 fs-6" href="mailto:<?php esc_html_e( 'info.mareksalamon@gmail.com', 'wpmf-themedev-v1' ); ?>" alt="<?php esc_html_e( 'Ikona', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'E-mail', 'wpmf-themedev-v1' ); ?>'" title="<?php esc_html_e( 'Napište mi zprávu', 'wpmf-themedev-v1' ); ?>">
							<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-envelope me-2 align-middle" fill="currentColor" viewBox="0 0 16 16">
								<path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4Zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2Zm13 2.383-4.708 2.825L15 11.105V5.383Zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741ZM1 11.105l4.708-2.897L1 5.383v5.722Z"/>
							</svg>
							<span class="d-inline align-middle"><?php esc_html_e( 'info.mareksalamon@gmail.com', 'wpmf-themedev-v1' ); ?></span>
						</a>
        			</div>
        			<div class="col-12 col-md-6 col-lg-4 px-0 pe-lg-5 pb-2 pb-md-5">
        				<h4><?php esc_html_e( 'Zavolejte', 'wpmf-themedev-v1' ); ?></h4>
						<a class="ps-0" href="tel:<?php esc_html_e( '00420739965521', 'wpmf-themedev-v1' ); ?>" alt="<?php esc_html_e( 'Ikona', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'Telefon', 'wpmf-themedev-v1' ); ?>'" title="<?php esc_html_e( 'Zavolejte na tel. číslo +420 739 965 521', 'wpmf-themedev-v1' ); ?>">
							<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-telephone me-2 align-middle" viewBox="0 0 16 16">
								<path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
							</svg>
							<span class="d-inline align-middle"><?php esc_html_e( '+420 739 965 521', 'wpmf-themedev-v1' ); ?></span>
						</a>
        			</div>
        			<div class="col-12 px-2 px-md-0 fs-smallest">
        					&copy; 2013&mdash;<?php echo date('Y'); ?> <?php echo '<a href="'.home_url().'">'.get_bloginfo('name').'</a>'; ?>. <?php esc_html_e( 'Všechna práva náleží jejich příslušným vlastníkům', 'wpmf-themedev-v1' ); ?>. <?php esc_html_e( 'Web vytvořil', 'wpmf-themedev-v1' ); ?> <a href="mailto:michal.fridrich@gmail.com" target="_blank" title="<?php esc_html_e( 'Kontaktujte tvůrce webu', 'wpmf-themedev-v1' ); ?>"> <?php esc_html_e( 'Michal Fridrich', 'wpmf-themedev-v1' ); ?></a>.
        			</div>
	            </div><!-- /.row -->
			</div><!-- /.container -->
		</footer><!-- /#footer -->
	</div><!-- /#wrapper -->

		<div id="hero-bg--slides" class="position-relative top-0 start-0 w-100 h-100"></div>
	<?php
		wp_footer();
	?>
	<?php get_template_part('js/hero', 'slides'); ?>
</body>
</html>

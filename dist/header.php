<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-touch-icon.png?v=1">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-32x32.png?v=1">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-16x16.png?v=1">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/site.webmanifest?v=1">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/safari-pinned-tab.svg?v=1" color="#000000">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon.ico?v=1">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="theme-color" content="#000000">

	<!-- HTML Meta Tags -->
	<meta name="description" content="Tvořím multimediální obsah – video produkci, kameru, střih, zvuk a hudbu. Mám více jak desetileté zkušenosti z celé řady projektů od reklamních po filmové produkce.">

	<!-- Facebook Meta Tags -->
	<meta property="og:url" content="https://www.mareksalamon.com/">
	<meta property="og:type" content="website">
	<meta property="og:title" content="Marek Salamon – Multimediální producent">
	<meta property="og:description" content="Tvořím multimediální obsah – video produkci, kameru, střih, zvuk a hudbu. Mám více jak desetileté zkušenosti z celé řady projektů od reklamních po filmové produkce.">
	<meta property="og:image" content="https://www.mareksalamon.com/wp-content/uploads/2023/01/www.mareksalamon-com-profile.jpg">

	<!-- Twitter Meta Tags -->
	<meta name="twitter:card" content="summary_large_image">
	<meta property="twitter:domain" content="mareksalamon.com">
	<meta property="twitter:url" content="https://www.mareksalamon.com/">
	<meta name="twitter:title" content="Marek Salamon – Multimediální producent">
	<meta name="twitter:description" content="Tvořím multimediální obsah – video produkci, kameru, střih, zvuk a hudbu. Mám více jak desetileté zkušenosti z celé řady projektů od reklamních po filmové produkce.">
	<meta name="twitter:image" content="https://www.mareksalamon.com/wp-content/uploads/2023/01/www.mareksalamon-com-profile.jpg">

	<?php wp_head(); ?>
</head>

<?php
	//$navbar_scheme   = get_theme_mod( 'navbar_scheme', 'navbar-light bg-light' ); // Get custom meta-value.
	//$navbar_position = get_theme_mod( 'navbar_position', 'static' ); // Get custom meta-value.

	$search_enabled  = get_theme_mod( 'search_enabled', '1' ); // Get custom meta-value.
?>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<a href="#main" class="visually-hidden-focusable"><?php esc_html_e( 'Skip to main content', 'wpmf-themedev-v1' ); ?></a>

<div id="wrapper">
	<header>
		<nav id="header" class="navbar navbar-expand-lg navbar-top p-2 p-md-5 fixed-top show<?php if ( is_home() || is_front_page() ) : echo ' home'; endif; ?>">
			<div class="container-fluid">
				<a class="navbar-brand header-logo" href="<?php echo esc_url( home_url() ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<svg id="mareksalamon-logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 106.29 20">
						<title>mareksalamon-com-logotype</title><g id="typo"><path d="M50.55,0h1.08l1.92,1.93L55.47,0h1.07V5.29H55.47V1.48L53.55,3.33,51.63,1.48V5.29H50.55Z"/><path d="M60,0h1.13l2.56,5.29H62.55l-.42-.89H59l-.41.89H57.5Zm1.72,3.51-1.15-2.4-1.11,2.4Z"/><path d="M64.64,0h2.69a3.7,3.7,0,0,1,1.06.13,2,2,0,0,1,.74.36,1.48,1.48,0,0,1,.43.59,1.94,1.94,0,0,1,.15.78,2.52,2.52,0,0,1-.07.56,1.67,1.67,0,0,1-.22.49,1.74,1.74,0,0,1-.37.4,2.21,2.21,0,0,1-.53.29l1.15,1.69H68.36l-1-1.51H65.71V5.29H64.64Zm2.72,2.85a1.7,1.7,0,0,0,.53-.07.92.92,0,0,0,.38-.2.79.79,0,0,0,.23-.31,1.07,1.07,0,0,0,.08-.41.79.79,0,0,0-.31-.68,1.37,1.37,0,0,0-.91-.25H65.71V2.85Z"/><path d="M71.12,0h4.5V.94H72.19V2h3v.89h-3V4.35h3.5v.94H71.12Z"/><path d="M77,0h1.07V2.74L80.88,0h1.37l-2.6,2.52,2.6,2.77H80.83l-2-2L78.09,4V5.29H77Z"/><path d="M50.8,11c.19.1.38.2.58.29a4.44,4.44,0,0,0,.61.22c.22.06.44.1.67.14a6,6,0,0,0,.75,0,4.62,4.62,0,0,0,.81-.06,2.11,2.11,0,0,0,.55-.17.78.78,0,0,0,.3-.26.58.58,0,0,0,.1-.33.52.52,0,0,0-.24-.46,1.33,1.33,0,0,0-.74-.17l-.46,0-.5.06-.49.07-.45,0a2.15,2.15,0,0,1-.68-.1A1.82,1.82,0,0,1,51,10.1a1.27,1.27,0,0,1-.4-.46A1.29,1.29,0,0,1,50.48,9a1.65,1.65,0,0,1,.06-.43,1.33,1.33,0,0,1,.19-.4,1.87,1.87,0,0,1,.33-.37,2.17,2.17,0,0,1,.5-.28,3,3,0,0,1,.68-.2,5.75,5.75,0,0,1,.87-.06q.36,0,.72,0l.7.11.65.17c.21.07.4.15.59.23l-.47.86-.49-.18-.53-.14a5.4,5.4,0,0,0-.58-.1c-.2,0-.41,0-.61,0a3.62,3.62,0,0,0-.73.06,1.88,1.88,0,0,0-.46.16.63.63,0,0,0-.23.23.44.44,0,0,0-.07.24.46.46,0,0,0,.21.39,1.16,1.16,0,0,0,.65.15l.41,0,.48-.05.51-.06.51,0a3,3,0,0,1,.83.1,1.69,1.69,0,0,1,.6.3,1.29,1.29,0,0,1,.37.47,1.61,1.61,0,0,1-.08,1.43,2,2,0,0,1-.61.61,3.24,3.24,0,0,1-.94.37,5.83,5.83,0,0,1-1.23.12,6.26,6.26,0,0,1-.87,0,7,7,0,0,1-.82-.16,5.85,5.85,0,0,1-.74-.25,5.75,5.75,0,0,1-.66-.31Z"/><path d="M59.42,7.32h1.13l2.56,5.29H62l-.43-.89H58.47l-.41.89H56.94Zm1.72,3.52L60,8.44l-1.12,2.4Z"/><path d="M64.07,7.32h1.07v4.35h3.08v.94H64.07Z"/><path d="M71.2,7.32h1.14l2.55,5.29H73.77l-.43-.89H70.26l-.41.89H68.72Zm1.72,3.52-1.14-2.4-1.11,2.4Z"/><path d="M75.86,7.32h1.07l1.92,1.93,1.92-1.93h1.07v5.29H80.77V8.81l-1.92,1.85L76.93,8.81v3.8H75.86Z"/><path d="M83.18,10a2.59,2.59,0,0,1,.22-1.09A2.41,2.41,0,0,1,84,8a2.84,2.84,0,0,1,1-.55,3.88,3.88,0,0,1,2.46,0,2.84,2.84,0,0,1,1,.55,2.41,2.41,0,0,1,.61.86A2.59,2.59,0,0,1,89.22,10,2.72,2.72,0,0,1,89,11.06a2.5,2.5,0,0,1-.61.86,2.86,2.86,0,0,1-1,.56,3.88,3.88,0,0,1-2.46,0,2.86,2.86,0,0,1-1-.56,2.5,2.5,0,0,1-.61-.86A2.72,2.72,0,0,1,83.18,10Zm1.07,0a1.78,1.78,0,0,0,.15.74,1.6,1.6,0,0,0,.4.56,2,2,0,0,0,.62.35,2.4,2.4,0,0,0,1.56,0,1.84,1.84,0,0,0,.61-.35,1.62,1.62,0,0,0,.41-.56,1.78,1.78,0,0,0,.15-.74A1.7,1.7,0,0,0,88,9.22a1.5,1.5,0,0,0-.41-.55A1.84,1.84,0,0,0,87,8.32a2.82,2.82,0,0,0-1.56,0,2,2,0,0,0-.62.35,1.48,1.48,0,0,0-.4.55A1.7,1.7,0,0,0,84.25,10Z"/><path d="M90.55,7.32h1.07l3.29,1.73V7.32H96v5.29H94.91V10.18L91.62,8.47v4.14H90.55Z"/><path d="M50.55,14.65h1.08l1.92,1.92,1.92-1.92h1.07v5.28H55.47v-3.8L53.55,18l-1.92-1.85v3.8H50.55Z"/><path d="M58,14.65h1.08v2.91a2.11,2.11,0,0,0,.09.64,1.35,1.35,0,0,0,.3.47,1.29,1.29,0,0,0,.48.29,1.93,1.93,0,0,0,.67.1,2,2,0,0,0,.67-.1,1.29,1.29,0,0,0,.48-.29A1.2,1.2,0,0,0,62,18.2a1.83,1.83,0,0,0,.1-.64V14.65h1.07v3a2.58,2.58,0,0,1-.17.94,2,2,0,0,1-.51.73,2.29,2.29,0,0,1-.82.47A3.5,3.5,0,0,1,60.6,20a3.41,3.41,0,0,1-1.11-.17,2.21,2.21,0,0,1-.82-.47,2,2,0,0,1-.51-.73,2.39,2.39,0,0,1-.18-.94Z"/><path d="M64.66,14.65h1.07V19H68.8v.94H64.66Z"/><path d="M71.58,15.59h-2v-.94h5v.94H72.65v4.34H71.58Z"/><path d="M75.75,14.65h1.07v5.28H75.75Z"/><path d="M78.33,14.65H79.4l1.93,1.92,1.92-1.92h1.07v5.28H83.25v-3.8L81.33,18,79.4,16.13v3.8H78.33Z"/><path d="M86,14.65h4.51v.94H87v1.05h3v.88H87V19h3.51v.94H86Z"/><path d="M91.87,14.65H93.8a5.4,5.4,0,0,1,1.48.18,2.82,2.82,0,0,1,1,.52,2.19,2.19,0,0,1,.61.83,2.86,2.86,0,0,1,.2,1.09,2.82,2.82,0,0,1-.2,1.06,2.23,2.23,0,0,1-.61.85,3,3,0,0,1-1,.55,4.68,4.68,0,0,1-1.48.21H91.87ZM94.07,19a2.65,2.65,0,0,0,.84-.13,1.61,1.61,0,0,0,.6-.35,1.31,1.31,0,0,0,.37-.55,2,2,0,0,0,.12-.71,1.9,1.9,0,0,0-.12-.7,1.34,1.34,0,0,0-.37-.54,1.73,1.73,0,0,0-.6-.33,2.66,2.66,0,0,0-.84-.12H92.94V19Z"/><path d="M98.21,14.65h1.08v5.28H98.21Z"/><path d="M102.59,14.65h1.14l2.56,5.28h-1.13l-.42-.89h-3.09l-.41.89h-1.13Zm1.73,3.51-1.15-2.4-1.11,2.4Z"/></g><polygon id="symbol" points="22.87 0 22.88 7.76 2.1 19.09 45.94 0 46 20 22.9 20 0.03 20 0 20 22.87 0"/></svg>
				</a>

				<button class="navbar-toggler p-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-primary" aria-controls="navbar" aria-expanded="false" aria-label="<?php esc_attr_e( 'Přepnout navigaci', 'wpmf-themedev-v1' ); ?>">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list me-0 navbar-toggler-open" viewBox="0 0 16 16">
						<path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
					</svg>
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg navbar-toggler-close" viewBox="0 0 16 16">
					  	<path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
					</svg>
				</button>

				<div id="navbar-primary" class="collapse navbar-collapse">
					<hr class="d-lg-none">
					<?php
					// Loading WordPress Custom Menu (theme_location).
					wp_nav_menu(
						array(
							'theme_location' => 'main-menu',
							'container'      => '',
							'menu_class'     => 'navbar-nav ms-auto me-auto mt-3 mt-lg-0',
							'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
							'walker'         => new WP_Bootstrap_Navwalker(),
							'add_li_class'  => 'mx-0 mx-lg-2'
						)
					); ?>
					<hr class="d-lg-none">
					<ul id="menu-primarni-vpravo" class="navbar-nav navbar-nav--primary mb-2 mb-lg-0 pt-3 pt-lg-0 pb-5 pb-lg-0">
						<li class="nav-item nav-item--icon py-md-0">
							<a class="nav-link font-heading px-md-2 px-xl-3" href="https://soundcloud.com/spectral_index" target="_blank" alt="<?php esc_html_e( 'Ikona', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'Soundcloud', 'wpmf-themedev-v1' ); ?>'" title="<?php esc_html_e( 'Soundcloud profil', 'wpmf-themedev-v1' ); ?>">
								<svg viewBox="0 0 50 23" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2" class="bi bi-vimeo me-2 me-lg-0 align-middle">
									<path d="M49.139 15.466a6.88 6.88 0 01-6.966 6.495H25.429a1.404 1.404 0 01-1.39-1.396V2.536a1.536 1.536 0 01.925-1.468S26.504 0 29.748 0a10.973 10.973 0 015.63 1.547 11.14 11.14 0 015.242 7.26 6.465 6.465 0 011.776-.242 6.703 6.703 0 016.744 6.901h-.001zm-27.19-11.79c.505 6.115.872 11.692 0 17.787a.544.544 0 01-1.081 0c-.813-6.043-.46-11.725 0-17.787a.542.542 0 01.83-.52c.176.11.274.313.25.52zM18.56 21.469a.569.569 0 01-1.127 0 67.976 67.976 0 010-15.695.57.57 0 011.134 0c.673 5.21.67 10.485-.007 15.695zM15.166 5.243c.55 5.603.8 10.623-.006 16.213a.545.545 0 01-1.088 0c-.78-5.518-.518-10.682 0-16.213.03-.28.266-.49.547-.49.28 0 .517.21.547.49zm-3.4 16.233a.559.559 0 01-1.108 0 57.407 57.407 0 010-14.654.563.563 0 111.127 0 53.55 53.55 0 01-.02 14.654zM8.37 10.486c.859 3.8.472 7.156-.032 11.029a.532.532 0 01-1.05 0c-.458-3.82-.838-7.255-.032-11.03a.558.558 0 011.114.001zm-3.388-.577c.787 3.893.53 7.189-.02 11.095-.065.577-1.054.583-1.107 0-.498-3.847-.734-7.242-.02-11.095a.578.578 0 011.147 0zm-3.42 1.887c.825 2.582.543 4.68-.033 7.327a.537.537 0 01-1.07 0c-.497-2.595-.7-4.738-.044-7.327a.575.575 0 011.146 0z" fill="currentColor"/>
								</svg>
								<span class="d-inline d-lg-none align-middle"><?php esc_html_e( 'Soundcloud', 'wpmf-themedev-v1' ); ?></span>
							</a>
						</li>
						<li class="nav-item nav-item--icon py-md-0">
							<a class="nav-link font-heading px-md-2 px-xl-3" href="https://vimeo.com/salamonmarek" target="_blank" alt="<?php esc_html_e( 'Ikona', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'Vimeo', 'wpmf-themedev-v1' ); ?>'" title="<?php esc_html_e( 'Vimeo kanál', 'wpmf-themedev-v1' ); ?>">
								<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-vimeo me-2 me-lg-0 align-middle" viewBox="0 0 16 16">
									<path d="M15.992 4.204c-.071 1.556-1.158 3.687-3.262 6.393-2.175 2.829-4.016 4.243-5.522 4.243-.933 0-1.722-.861-2.367-2.583L3.55 7.523C3.07 5.8 2.556 4.94 2.007 4.94c-.118 0-.537.253-1.254.754L0 4.724a209.56 209.56 0 0 0 2.334-2.081c1.054-.91 1.845-1.388 2.373-1.437 1.243-.123 2.01.728 2.298 2.553.31 1.968.526 3.19.646 3.666.36 1.631.756 2.446 1.186 2.445.334 0 .836-.53 1.508-1.587.671-1.058 1.03-1.863 1.077-2.415.096-.913-.263-1.37-1.077-1.37a3.022 3.022 0 0 0-1.185.261c.789-2.573 2.291-3.825 4.508-3.756 1.644.05 2.419 1.117 2.324 3.2z"/>
								</svg>
								<span class="d-inline d-lg-none align-middle"><?php esc_html_e( 'Vimeo', 'wpmf-themedev-v1' ); ?></span>
							</a>
						</li>
						<li class="nav-item nav-item--icon py-md-0">
							<a class="nav-link font-heading px-md-2 px-xl-3" href="https://www.youtube.com/user/Neurochirurgy" target="_blank" alt="<?php esc_html_e( 'Ikona', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'YouTube', 'wpmf-themedev-v1' ); ?>'" title="<?php esc_html_e( 'YouTube kanál', 'wpmf-themedev-v1' ); ?>">
								<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-youtube me-2 me-lg-0 align-middle" viewBox="0 0 16 16">
									<path d="M8.051 1.999h.089c.822.003 4.987.033 6.11.335a2.01 2.01 0 0 1 1.415 1.42c.101.38.172.883.22 1.402l.01.104.022.26.008.104c.065.914.073 1.77.074 1.957v.075c-.001.194-.01 1.108-.082 2.06l-.008.105-.009.104c-.05.572-.124 1.14-.235 1.558a2.007 2.007 0 0 1-1.415 1.42c-1.16.312-5.569.334-6.18.335h-.142c-.309 0-1.587-.006-2.927-.052l-.17-.006-.087-.004-.171-.007-.171-.007c-1.11-.049-2.167-.128-2.654-.26a2.007 2.007 0 0 1-1.415-1.419c-.111-.417-.185-.986-.235-1.558L.09 9.82l-.008-.104A31.4 31.4 0 0 1 0 7.68v-.123c.002-.215.01-.958.064-1.778l.007-.103.003-.052.008-.104.022-.26.01-.104c.048-.519.119-1.023.22-1.402a2.007 2.007 0 0 1 1.415-1.42c.487-.13 1.544-.21 2.654-.26l.17-.007.172-.006.086-.003.171-.007A99.788 99.788 0 0 1 7.858 2h.193zM6.4 5.209v4.818l4.157-2.408L6.4 5.209z"/>
								</svg>
								<span class="d-inline d-lg-none align-middle"><?php esc_html_e( 'YouTube', 'wpmf-themedev-v1' ); ?></span>
							</a>
						</li>
						<li class="nav-item nav-item--icon py-md-0">
							<a class="nav-link font-heading px-md-2 px-xl-3" href="tel:<?php esc_html_e( '00420739965521', 'wpmf-themedev-v1' ); ?>" alt="<?php esc_html_e( 'Ikona', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'Telefon', 'wpmf-themedev-v1' ); ?>'" title="<?php esc_html_e( 'Zavolejte', 'wpmf-themedev-v1' ); ?>">
								<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-telephone me-2 me-lg-0 align-middle" viewBox="0 0 16 16">
									<path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
								</svg>
								<span class="d-inline d-lg-none align-middle"><?php esc_html_e( '+420 739 965 521', 'wpmf-themedev-v1' ); ?></span>
							</a>
						</li>
						<li class="nav-item nav-item--icon py-md-0">
							<a class="nav-link font-heading px-md-2 px-xl-3" href="mailto:<?php esc_html_e( 'info.mareksalamon@gmail.com', 'wpmf-themedev-v1' ); ?>" alt="<?php esc_html_e( 'Ikona', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'E-mail', 'wpmf-themedev-v1' ); ?>'" title="<?php esc_html_e( 'Napište mi zprávu', 'wpmf-themedev-v1' ); ?>">
								<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-envelope me-2 me-lg-0 align-middle" viewBox="0 0 16 16">
									<path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4Zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2Zm13 2.383-4.708 2.825L15 11.105V5.383Zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741ZM1 11.105l4.708-2.897L1 5.383v5.722Z"/>
								</svg>
								<span class="d-inline d-lg-none align-middle"><?php esc_html_e( 'Napište', 'wpmf-themedev-v1' ); ?></span>
							</a>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->

			</div><!-- /.container -->
		</nav>
	</header><!-- /#header -->

	<section id="hero<?php if ( is_home() || is_front_page() ) : echo '-home'; endif; ?>" class="hero-wrapper hero-wrapper<?php if ( is_home() || is_front_page() ) : echo '--home'; endif; ?>">
		<div id="hero-bg--slides" class="hero-slides--homepage position-absolute top-0 start-0 w-100 h-100"></div>
		<div class="container-fluid position-absolute bottom-0 start-0">
			<div class="row p-2 p-sm-3 p-md-5">
				<div class="col-12 col-md-6">
					<h1 class="pb-3">
						<?php if ( is_home() || is_front_page() || is_archive() ) : ?>
						<!--Title Home-->
						<?php the_title(); ?>
						<?php endif; ?>


						<?php if ( is_single() ) : ?>
						<!--Title Single -->
							<?php if ( get_field( 'odkaz_na_vimeo_video' ) ):
							$post_link = get_field('odkaz_na_vimeo_video');
							else :
							$post_link = get_field('odkaz_na_youtube_video');
							endif; ?>
							<?php if ( get_field( 'odkaz_na_vimeo_video' ) || get_field( 'odkaz_na_youtube_video' ) ): ?>
							<a href="<?php echo $post_link; ?>" data-lity>
							<?php the_title(); ?>
							</a>
							<?php else : ?>
							<?php the_title(); ?>
							<?php endif; ?>


				    		<?php if ( get_field( 'odkaz_na_vimeo_video' ) || get_field( 'odkaz_na_youtube_video' ) ): ?>
								<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-play-circle ms-1 me-0" viewBox="0 0 16 16">
									<path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
									<path d="M6.271 5.055a.5.5 0 0 1 .52.038l3.5 2.5a.5.5 0 0 1 0 .814l-3.5 2.5A.5.5 0 0 1 6 10.5v-5a.5.5 0 0 1 .271-.445z"/>
								</svg>
							<?php endif; ?>
						<?php endif; ?>

					</h1>
					<div class="pb-0 pb-md-3">
						<?php if ( is_home() || is_front_page() ) : ?>
						<p class="pb-3">
						<?php the_content(); ?>
						<? else : ?>
						<?php $popisek = get_field('popisek');
						echo $popisek; ?>
						<? endif; ?>
					</div>
					<?php if ( is_home() || is_front_page() ) : ?>
					<div class="nav-item nav-item--icon py-3 py-md-0">
						<a class="nav-link font-heading ps-0" href="#main" alt="<?php esc_html_e( 'Ikona', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'Showreel', 'wpmf-themedev-v1' ); ?>'" title="<?php esc_html_e( 'Zobrazit reference', 'wpmf-themedev-v1' ); ?>">
							<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-arrow-down-circle align-middle" viewBox="0 0 16 16">
								  <path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v5.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V4.5z"/>
							</svg>
							<span class="d-inline align-middle"><?php esc_html_e( 'Portfolio', 'wpmf-themedev-v1' ); ?></span>
						</a>
					</div>
					<? endif; ?>
					<?php if ( is_single() ) : ?>
					<div class="nav-item nav-item--icon py-3 py-md-0">
						<a class="nav-link font-heading ps-0" href="#main" alt="<?php esc_html_e( 'Ikona', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'Info', 'wpmf-themedev-v1' ); ?>'" title="<?php esc_html_e( 'Zobrazit reference', 'wpmf-themedev-v1' ); ?>">
							<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-text-left align-middle" viewBox="0 0 16 16">
								<path fill-rule="evenodd" d="M2 12.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/>
							</svg>
							<span class="d-inline align-middle"><?php esc_html_e( 'Info', 'wpmf-themedev-v1' ); ?></span>
						</a>
					</div>
					<? endif; ?>
				</div>
			</div>
		</div>
	</section>

	<main id="main">
		<?php
			// If Single or Archive (Category, Tag, Author or a Date based page).
			if ( is_single() || is_archive() ) :
		?>
			<div class="row">
				<div class="col-12">
		<?php
			endif;
		?>

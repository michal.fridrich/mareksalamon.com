<?php
if ( is_front_page() || is_home() ) {
    $slides_wrapper = "hero-bg--slides";
} else {
    $slides_wrapper = "banner-bg--solutions";
};

?>
<script>
    jQuery('#<?php echo $slides_wrapper; ?>').vegas({
        overlay: false,
        transition: 'blur',
        transitionDuration: 10000,
        delay: 10000,
        color: 'black',
        loop: true,
        animation: 'kenburns',
        animationDuration: 15000,
        slides: [
        <?php
        $args = array(
            'posts_per_page' => -1,
            'post_type' => 'post',
            'orderby' => 'date',
            'order' => 'DESC',
        );
        query_posts($args);
        while ( have_posts() ) : the_post();
            if (!has_post_thumbnail( $post->ID ) ):
                $video_link = get_field('work_youtube');
                $video_link_key = str_replace("https://www.youtube.com/watch?v=", "", $video_link);
                $featured_img_url = "https://img.youtube.com/vi/" . $video_link_key . "/maxresdefault.jpg";
            else:
                $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'medium');
            endif;
            ?>

            { src: "<?php echo $featured_img_url; ?>" },

        <?php
        endwhile;
        ?>
      ]
    });
</script>
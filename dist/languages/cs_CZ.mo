��    [      �     �      �     �  
   �     �     �     
  (     %   6  -  \     �	  ?   �	     �	     �	  
   �	      
  
   
     
     .
     6
     I
  5   ^
     �
     �
     �
     �
     �
     �
     �
     �
     �
  D   �
  1   ;     m     �  a   �     �                    "     8  
   D     O  	   `     j     x     �     �     �     �     �     �  $   �     �     �  	                  '     /     <  M   B     �     �     �     �  _   �     5     <     C  P   K  �   �  �   "  �   �     N     `     s     �     �  9   �  )   �  $     R   -  ,   �     �     �      �     �     �  #        '    E  
   �  
   �  ,   �          $  3   '  '   [  2  �     �  ;   �     �     	          "     5     =  
   P     [     u  ;   �     �     �  	   �     �     �     �               -  0   ?     p     �     �  k   �     &     =     D     W     r     �     �     �  
   �     �     �     	  	             1  	   K     U     n     �     �     �     �     �  
   �     �     �  ^        f     o     �     �  g   �  	   0     :     B  >   O  �   �  �     �   �     ]     n  $   �     �     �  F   �  ,      '   E   o   m   #   �      !      !     ?!     C!     I!  
   \!     g!                      G   U   =       B      E      R       V   X   M           ,                 .       A      -      N   >   P              !                  Z              @   F          Y   C           L   ;   [   /              W   2      T       7   "          0         <   '   ?   	   (   8   S       4              9      :   5   %      O   6   $       #           &          K   )           Q   +       3                       J   1   D   I             H   *   
        %1$s ago %1$s, %2$s %s Comment %s Comments &larr; Older Comments ,  <span class="%1$s">Posted in</span> %2$s <span class="%1$s">Tagged</span> %2$s <span class="sep">Posted on </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a><span class="by-author"> <span class="sep"> by </span> <span class="author-meta vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span> About %s Apologies, but no results were found for the requested archive. Author Archives: %s Blog Archives Blog Index Cancel reply Categories Category Archives: %s Comment Comment navigation Comments are closed. Continue reading <span class="meta-nav">&rarr;</span> Daily Archives: %s Dark Default Edit Email Featured Fixed to bottom Fixed to top Height: &gt;80px I'm an Open-source Starter Theme developed and maintained by them.es It looks like nothing was found at this location. Leave a Reply to %s Leave a comment Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a> Monthly Archives: %s Name Navbar Navbar Scheme Newer Comments &rarr; Newer posts Next image No Comments yet! Not found Nothing Found Older posts Page Page  Page (Default) Page (Full width) Pages: Parent post Parent post linkPublished in %title Password Permalink to %s Pingback: Post Comment Previous image Primary Recent Posts Reply Save my name, email, and website in this browser for the next time I comment. Search Search Results for: %s Show Searchfield? Skip to main content Sorry, but nothing matched your search criteria. Please try again with some different keywords. Static Submit Tag: %s This content is password protected. To view it please enter your password below. This entry was posted by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>. This entry was posted in %1$s and tagged %2$s by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>. This entry was posted in %1$s by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>. Toggle navigation Upload Header Logo View all posts by %s Website Yearly Archives: %s You must be <a href="%s">logged in</a> to post a comment. Your Email address will not be published. Your comment is awaiting moderation. comments title%1$s Reply to &ldquo;%2$s&rdquo; %1$s Replies to &ldquo;%2$s&rdquo; comments titleOne Reply to &ldquo;%s&rdquo; https://them.es https://them.es/starter monthly archives date formatF Y more them.es them.es Starter Theme (Bootstrap 4) yearly archives date formatY Project-Id-Version: them.es Starter Theme v1.0
Report-Msgid-Bugs-To: 
POT-Revision-Date: Tue Apr 12 2016 16:05:52 GMT+0200 (CEST)
PO-Revision-Date: 2021-06-27 22:04+0200
Language-Team: 
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n>=2 && n<=4 ? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 3.0
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Loco-Target-Locale: de_DE
Last-Translator: 
Language: cs_CZ
X-Poedit-SearchPath-0: .
 Před %1$s %1$s, %2$s %s komentář %s komentáře %s komentářů &larr; Starší komentáře ,  <span class="%1$s">Přidáno do rubriky</span> %2$s <span class="%1$s">Štítky</span> %2$s <span class="sep">Přidáno </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a><span class="by-author"> <span class="sep"> autorem </span> <span class="author-meta vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span> O %s Omlouváme se, ale pro hledaný archiv nebylo nic nalezeno. Archiv autora: %s Archiv blogu Index blogu Zrušit komentář Rubriky Archivy rubrik: %s Komentář Procházení komentářů Komentáře jsou uzavřeny. Pokračovat ve čtení <span class="meta-nav">&rarr;</span> Denní archiv: %s Tmavá Výchozí Upravit E-mail Zvýrazněno Plovoucí dole Plovoucí nahoře Výška: &gt;80px Šablona pro Wordpress od www.michalfridrich.com Zdá se, že zde nic není. Zanechat komentář pro %s Přidat komentář Přihlášen jako <a href="%1$s">%2$s</a>. <a href="%3$s" title="Odhlásit se z toho účtu">Odhlásit?</a> Měsíční archiv: %s Jméno Navigační lišta Schéma navigační lišty Novější komentáře &rarr; Novější příspěvky Další obrázek Prozatím žádné komentáře! Nenalezeno Nic nenalezeno Starší příspěvky Stránka Stránka  Stránka (výchozí) Stránka (plná šířka) Stránky: Nadřazený příspěvek Published in %title Heslo Trvalý odkaz pro %s Zpětný odkaz: Přidat komentář Předchozí obrázek Primární Nejnovější příspěvky Odpovědět Přeji si uložit jméno, e-mail a adresu webu v tomto prohlížeči pro budoucí komentáře. Vyhledat Výsledky hledání pro: %s Zobrazovat vyhledávací panel? Přeskočit na hlavní obsah Omlouváme se, ale žádný obsah neodpovídá vašemu hledání. Zkuste zvolit jiná klíčová slova. Statická Odeslat Štítek: %s Tento obsah je chráněn heslem. Pro zobrazení zadejte heslo. Přidáno do rubriky <a href="%6$s">%5$s</a>. Uložte si <a href="%3$s" title="Trvalý odkaz pro %4$s" rel="bookmark">trvalý odkaz</a>. Přidáno do rubriky %1$s a označeno štítky %2$s autorem <a href="%6$s">%5$s</a>. Uložte si <a href="%3$s" title="Trvalý odkaz pro %4$s" rel="bookmark">trvalý odkaz</a>. Přidáno do rubriky %1$s autorem <a href="%6$s">%5$s</a>. Uložte si <a href="%3$s" title="Trvalý odkaz pro %4$s" rel="bookmark">trvalý odkaz</a>. Zapnout navigaci Nahrát logo do hlavičky Zobrazit všechny příspěvky od %s Webová stránka Roční archiv: %s Musíte být <a href="%s">přihlášen</a> pro přidání komentáře. Vaše e-mailová adresa nebude publikována. Váš komentář čeká na schválení. %1$s komentář pro &ldquo;%2$s&rdquo; %1$s komentáře &ldquo;%2$s&rdquo; %1$s komentářů &ldquo;%2$s&rdquo; Jeden komentář k &ldquo;%s&rdquo; https://www.michalfridrich.com https://www.michalfridrich.com F Y více michalfridrich.com WPMF Theme Y 
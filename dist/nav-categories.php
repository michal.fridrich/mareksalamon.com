<h2 id="portfolio-categories--title" class="pb-3"><?php esc_html_e( 'Moje práce', 'wpmf-themedev-v1' ); ?></h2>
<!-- NEW ADD 25.09.2022 -->

<!-- FILTER: video-hudebni -->
<a href="#portfolio-home--wrapper" title="<?php esc_html_e( 'Filtrovat reference z', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'Hudební video', 'wpmf-themedev-v1' ); ?>'" class="font-heading text-uppercase align-middle portfolio-categories--item py-2" data-category="video-hudebni" onclick="filterPortfolio()" data-bs-toggle="collapse" data-bs-target="#navbar-secondary">
    <?php esc_html_e( 'Hudební video', 'wpmf-themedev-v1' ); ?>
</a>
<!-- FILTER: sound-design -->
<a href="#portfolio-home" title="<?php esc_html_e( 'Filtrovat reference z', 'wpmf-themedev-v1' ); ?> 'Sound design'" class="font-heading text-uppercase align-middle portfolio-categories--item py-2" data-category="sound-design" onclick="filterPortfolio()" data-bs-toggle="collapse" data-bs-target="#navbar-secondary">
    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-soundwave" viewBox="0 0 16 16">
        <path fill-rule="evenodd" d="M8.5 2a.5.5 0 0 1 .5.5v11a.5.5 0 0 1-1 0v-11a.5.5 0 0 1 .5-.5zm-2 2a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zm4 0a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zm-6 1.5A.5.5 0 0 1 5 6v4a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm8 0a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm-10 1A.5.5 0 0 1 3 7v2a.5.5 0 0 1-1 0V7a.5.5 0 0 1 .5-.5zm12 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0V7a.5.5 0 0 1 .5-.5z"/>
    </svg>
    <?php esc_html_e( 'Sound design', 'wpmf-themedev-v1' ); ?>
</a>
<!-- FILTER: video-kamera -->
<a href="#portfolio-home" title="<?php esc_html_e( 'Filtrovat reference z', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'Kamera', 'wpmf-themedev-v1' ); ?>'" class="font-heading text-uppercase align-middle portfolio-categories--item py-2" data-category="video-kamera" onclick="filterPortfolio()" data-bs-toggle="collapse" data-bs-target="#navbar-secondary">
    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-camera-video" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M0 5a2 2 0 0 1 2-2h7.5a2 2 0 0 1 1.983 1.738l3.11-1.382A1 1 0 0 1 16 4.269v7.462a1 1 0 0 1-1.406.913l-3.111-1.382A2 2 0 0 1 9.5 13H2a2 2 0 0 1-2-2V5zm11.5 5.175 3.5 1.556V4.269l-3.5 1.556v4.35zM2 4a1 1 0 0 0-1 1v6a1 1 0 0 0 1 1h7.5a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1H2z"/>
    </svg>
    <?php esc_html_e( 'Kamera', 'wpmf-themedev-v1' ); ?>
</a>
<!-- FILTER: divadlo -->
<a href="#portfolio-home" title="<?php esc_html_e( 'Filtrovat reference z', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'Divadlo', 'wpmf-themedev-v1' ); ?>'" class="font-heading text-uppercase align-middle portfolio-categories--item py-2" data-category="divadlo" onclick="filterPortfolio()" data-bs-toggle="collapse" data-bs-target="#navbar-secondary">
    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-music-note-beamed" viewBox="0 0 16 16">
        <path d="M6 13c0 1.105-1.12 2-2.5 2S1 14.105 1 13c0-1.104 1.12-2 2.5-2s2.5.896 2.5 2zm9-2c0 1.105-1.12 2-2.5 2s-2.5-.895-2.5-2 1.12-2 2.5-2 2.5.895 2.5 2z"/>
        <path fill-rule="evenodd" d="M14 11V2h1v9h-1zM6 3v10H5V3h1z"/>
        <path d="M5 2.905a1 1 0 0 1 .9-.995l8-.8a1 1 0 0 1 1.1.995V3L5 4V2.905z"/>
    </svg>
    <?php esc_html_e( 'Divadlo', 'wpmf-themedev-v1' ); ?>
</a>
<!-- FILTER CANCEL -->
<a href="#portfolio-home" title="Zrušit filtr" class="font-heading text-uppercase align-middle portfolio-categories--item py-2 portfolio-categories--cancel" onclick="filterPortfolioCancel()" data-bs-toggle="collapse" data-bs-target="#navbar-secondary">
    <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
        <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
    </svg>
    <?php esc_html_e( 'Zrušit filtr', 'wpmf-themedev-v1' ); ?>
</a>
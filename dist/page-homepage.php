<?php
/**
 * Template Name: Page (Homepage)
 * Description: Page template with Sidebar on the left side.
 *
 */

get_header();
get_template_part('archive');
get_template_part('section', 'music-rental');
//get_template_part('section', 'about');
get_template_part('section', 'clients');
get_footer();

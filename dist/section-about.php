<?php
/*if (get_locale() == 'en_US') {
    $id=232;
} else {
    $id=190;
};*/
wp_reset_postdata();
$post_id = 784;
$post_about = get_post($post_id);
$title_about = apply_filters('the_title', $post_about->post_title);
$content_about = apply_filters('the_content', $post_about->post_content);
$featured_img_url_about = get_the_post_thumbnail_url($post_about,'medium');
?>

<section id="about">
    <div class="container-fluid position-relative px-0">
        <div class="row mx-0 p-3 p-sm-4 p-md-6 g-2 g-lg-10">
            <div class="col-12 col-lg-6 px-0 mb-4 mb-lg-0">
                <img class="img-fluid" src="<?php echo $featured_img_url_about; ?>">
            </div>
            <div class="col-12 col-lg-6">
                <h3 class="pb-5">
                    <?php echo $title_about; ?>
                </h3>
                <?php echo $content_about; ?>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<?php wp_reset_postdata(); ?>
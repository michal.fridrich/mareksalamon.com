<section id="clients" class="">
    <div class="container-fluid position-relative px-0">
        <div class="row mx-0 p-3 p-sm-4 p-md-6 g-5">
            <div class="col-12 col-lg-4 px-0 pe-lg-3">
                <h3 class="pe-0 pe-md-5">
                    <?php esc_html_e( 'Spolupracoval jsem na projektech pro celou řadu klientů', 'wpmf-themedev-v1' ); ?>
                </h3>
            </div>
            <div class="col-12 col-lg-8 px-0 pe-lg-5 pb-5">
                <div class="container-fluid px-0">
                <div class="row g-6 mx-0">
                <?php
                $args = array(
                    'post_type' => 'klient',
                    'posts_per_page' => -1
                );
                $post_query = new WP_Query($args);
                if($post_query->have_posts() ) : {
                    while($post_query->have_posts() ) {
                        $post_query->the_post();
                        $id = get_the_ID();
                        $clientLogoImg = pods_field_display( 'client_logo' );
                        $clientWeb = get_post_meta($post->ID, 'client_web', true);
                    ?>

                    <div id="post-id-<?php echo $id; ?>" class="post-id-<?php echo $id; ?> client-item col-6 col-sm-3 col-md-2 justify-content-center text-center" title="<?php the_title(); ?>">
                        <a href="<?php echo $clientWeb; ?>" title="<?php the_title(); ?>" target="_blank" class="logo-client d-block">
                            <img src="<?php echo $clientLogoImg; ?>" class="img-fluid" alt="<?php esc_html_e( 'Logo firmy', 'wpmf-themedev-v1' ); ?> '<?php the_title(); ?>'">
                        </a>
                    </div>


                <?php } // endwhile
                } //have_post
                endif;
                wp_reset_postdata(); ?>

                </div> <!-- END .row -->
            </div>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
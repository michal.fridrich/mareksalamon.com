<div class="container-fluid position-relative px-0 mb-5 mb-lg-8">
    <div class="row mx-0">
        <?php
        /*if (get_locale() == 'en_US') {
            $id=232;
        } else {
            $id=190;
        };*/
        wp_reset_postdata();
        $post_id = 801;
        $post_music = get_post($post_id);
        $title_music = apply_filters('the_title', $post_music->post_title);
        $content_music = apply_filters('the_content', $post_music->post_content);
        $featured_img_url_music = get_the_post_thumbnail_url($post_music,'medium');
        $description_music = apply_filters('page_description', $post_music->page_description);
        ?>
        <div id="music" class="col-12 col-lg-6 p-3 p-sm-6 position-relative" style="background-image: url('<?php echo $featured_img_url_music; ?>');">
            <div class="position-absolute top-0 start-0 w-100 h-100 overlay-gradient-black-down"></div>
            <div class="ratio ratio-4x3">
                <div class="page-item-content position-relative">
                    <div class="page-item-content-inner">
                        <h3><?php echo $title_music; ?></h3>
                        <?php
                        echo $description_music; ?>
                        <button class="button-outline d-inline-block mb-3 me-2"
                         type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvas-music" aria-controls="offcanvas-music">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-headphones me-2 align-middle" viewBox="0 0 16 16">
                                <path d="M8 3a5 5 0 0 0-5 5v1h1a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V8a6 6 0 1 1 12 0v5a1 1 0 0 1-1 1h-1a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1h1V8a5 5 0 0 0-5-5z"/>
                            </svg>
                            <?php esc_html_e( 'Poslouchat zde na webu', 'wpmf-themedev-v1' ); ?>
                        </button>
                        <a class="button-outline d-inline-block mb-3" href="https://soundcloud.com/spectral_index" target="_blank" alt="<?php esc_html_e( 'Ikona', 'wpmf-themedev-v1' ); ?> '<?php esc_html_e( 'Soundcloud', 'wpmf-themedev-v1' ); ?>'" title="<?php esc_html_e( 'Soundcloud profil', 'wpmf-themedev-v1' ); ?>">
                            <svg viewBox="0 0 50 23" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2" class="bi bi-vimeo me-2 align-middle">
                                <path d="M49.139 15.466a6.88 6.88 0 01-6.966 6.495H25.429a1.404 1.404 0 01-1.39-1.396V2.536a1.536 1.536 0 01.925-1.468S26.504 0 29.748 0a10.973 10.973 0 015.63 1.547 11.14 11.14 0 015.242 7.26 6.465 6.465 0 011.776-.242 6.703 6.703 0 016.744 6.901h-.001zm-27.19-11.79c.505 6.115.872 11.692 0 17.787a.544.544 0 01-1.081 0c-.813-6.043-.46-11.725 0-17.787a.542.542 0 01.83-.52c.176.11.274.313.25.52zM18.56 21.469a.569.569 0 01-1.127 0 67.976 67.976 0 010-15.695.57.57 0 011.134 0c.673 5.21.67 10.485-.007 15.695zM15.166 5.243c.55 5.603.8 10.623-.006 16.213a.545.545 0 01-1.088 0c-.78-5.518-.518-10.682 0-16.213.03-.28.266-.49.547-.49.28 0 .517.21.547.49zm-3.4 16.233a.559.559 0 01-1.108 0 57.407 57.407 0 010-14.654.563.563 0 111.127 0 53.55 53.55 0 01-.02 14.654zM8.37 10.486c.859 3.8.472 7.156-.032 11.029a.532.532 0 01-1.05 0c-.458-3.82-.838-7.255-.032-11.03a.558.558 0 011.114.001zm-3.388-.577c.787 3.893.53 7.189-.02 11.095-.065.577-1.054.583-1.107 0-.498-3.847-.734-7.242-.02-11.095a.578.578 0 011.147 0zm-3.42 1.887c.825 2.582.543 4.68-.033 7.327a.537.537 0 01-1.07 0c-.497-2.595-.7-4.738-.044-7.327a.575.575 0 011.146 0z" fill="currentColor"/>
                            </svg>
                            <?php esc_html_e( 'Soundcloud', 'wpmf-themedev-v1' ); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div><!-- /.container -->

        <div class="offcanvas offcanvas-end mt-0 w-100 bg-secondary px-0 px-md-3 px-lg-5" tabindex="-1" id="offcanvas-music" aria-labelledby="title-music">
            <div class="offcanvas-bg position-absolute top-0 start-0 w-100 h-100" style="background-image: url('<?php echo $featured_img_url_music; ?>');">
            </div>
            <div class="offcanvas-header position-relative pe-0 py-md-5">
                <h5 class="offcanvas-title" id="title-music">
                    <?php echo $title_music; ?>
                </h5>
                <button type="button" class="btn btn-lg text-reset has-shadow offcanvas-close d-none d-md-block" data-bs-dismiss="offcanvas" aria-label="Zavřít">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                        <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
                    </svg>
                </button>
            </div>
            <div class="offcanvas-body position-relative pt-0 pb-5 px-0 px-md-3 ">
                <div class="container-fluid h-100 px-0">
                    <div class="row h-100 mx-0">
                        <div class="col-12 col-lg-5 px-3 px-md-0 pe-lg-5 mb-3 mb-lg-0">
                            <div class="color-primary mb-5 sticky-top lead">
                                <?php echo $description_music; ?>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7 px-3 px-md-0 lay-col-2">
                            <?php echo $content_music; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="offcanvas-footer position-relative py-3 py-md-5 px-3 d-md-none">
                <div class="container-fluid px-0">
                    <div class="row mx-0">
                        <div class="col-auto text-center ps-0 d-md-none ms-auto">
                            <button type="button" class="btn p-2 text-reset has-shadow offcanvas-close" data-bs-dismiss="offcanvas" aria-label="Zavřít">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                                    <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php wp_reset_postdata(); ?>
        <?php
        /*if (get_locale() == 'en_US') {
            $id=232;
        } else {
            $id=190;
        };*/
        wp_reset_postdata();
        $post_id = 803;
        $post_rental = get_post($post_id);
        $title_rental = apply_filters('the_title', $post_rental->post_title);
        $content_rental = apply_filters('the_content', $post_rental->post_content);
        $featured_img_url_rental = get_the_post_thumbnail_url($post_rental,'medium');
        $description_rental = apply_filters('page_description', $post_rental->page_description);
        ?>
        <div id="rental" class="col-12 col-lg-6 p-3 p-sm-6 position-relative" style="background-image: url('<?php echo $featured_img_url_rental; ?>');">
            <div class="position-absolute top-0 start-0 w-100 h-100 overlay-gradient-black-down"></div>
            <div class="ratio ratio-4x3">
                <div class="page-item-content position-relative">
                    <div class="page-item-content-inner">
                        <h3><?php echo $title_rental; ?></h3>
                        <?php echo $description_rental; ?>
                        <button class="button-outline d-inline-block mb-3 me-2"
                         type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvas-rental" aria-controls="offcanvas-rental">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-mic-fill me-2 align-middle" viewBox="0 0 16 16">
                                <path d="M5 3a3 3 0 0 1 6 0v5a3 3 0 0 1-6 0V3z"/>
                                <path d="M3.5 6.5A.5.5 0 0 1 4 7v1a4 4 0 0 0 8 0V7a.5.5 0 0 1 1 0v1a5 5 0 0 1-4.5 4.975V15h3a.5.5 0 0 1 0 1h-7a.5.5 0 0 1 0-1h3v-2.025A5 5 0 0 1 3 8V7a.5.5 0 0 1 .5-.5z"/>
                            </svg>
                            <?php esc_html_e( 'Seznam a ceník techniky', 'wpmf-themedev-v1' ); ?>
                        </button>
                    </div>
                </div>
            </div>
        </div><!-- /.container -->

        <div class="offcanvas offcanvas-end mt-0 w-100 bg-secondary px-0 px-md-3 px-lg-5" tabindex="-1" id="offcanvas-rental" aria-labelledby="title-rental">
            <div class="offcanvas-bg position-absolute top-0 start-0 w-100 h-100" style="background-image: url('<?php echo $featured_img_url_rental; ?>');">
            </div>
            <div class="offcanvas-header position-relative pe-0 py-md-5">
                <h5 class="offcanvas-title" id="title-rental">
                    <?php echo $title_rental; ?>
                </h5>
                <button type="button" class="btn btn-lg text-reset has-shadow offcanvas-close d-none d-md-block" data-bs-dismiss="offcanvas" aria-label="Zavřít">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                        <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
                    </svg>
                </button>
            </div>
            <div class="offcanvas-body position-relative pt-0 pb-5 px-0 px-md-3 ">
                <div class="container-fluid h-100 px-0">
                    <div class="row h-100 mx-0">
                        <div class="col-12 col-lg-5 px-3 px-md-0 pe-lg-5 mb-3 mb-lg-0">
                            <div class="color-primary mb-5 sticky-top lead">
                                <?php echo $description_rental; ?>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7 px-3 px-md-0">
                            <?php echo $content_rental; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="offcanvas-footer position-relative py-3 py-md-5 px-3 d-md-none">
                <div class="container-fluid px-0">
                    <div class="row mx-0">
                        <div class="col-auto text-center ps-0 d-md-none ms-auto">
                            <button type="button" class="btn p-2 text-reset has-shadow offcanvas-close" data-bs-dismiss="offcanvas" aria-label="Zavřít">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                                    <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php wp_reset_postdata(); ?>
    </div>
</div>
// Gulp
const gulp = require('gulp');

// Env
const mode = require('gulp-mode')({
  modes: ["development", "stage", "production"],
  default: "development",
  verbose: false
});

// CSS
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const googlefonts = require('gulp-google-fonts');

// JS
const jsImport = require('gulp-js-import');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

// Utils
const sourcemaps = require('gulp-sourcemaps');
const gutil = require('gulp-util');
const merge = require('merge-stream');
const imagemin = require('gulp-imagemin');
const ftp = require('vinyl-ftp');
const ftpconfig = require('./ftpcred');

// Live
//const browserSync = require('browser-sync').create();

// Paths
const path = {
    src: './src/',
    dest: './dist/',
    styles: {
        src: './src/scss/',
        dest: './dist/css'
    },
    scripts: {
        src: './src/js/',
        dest: './dist/js'
    },
    images: {
        src: './src/assets/img/',
        dest: './dist/assets/img'
    },
    remote: {
        host: ftpconfig.remote.host,
        dest: ftpconfig.remote.dest,
        user: ftpconfig.remote.user,
        pass: ftpconfig.remote.pass,
    }
};

// Watchers
gulp.task('watch', function(){
    // Modes
    var isProduction = mode.production();
    var isStage = mode.stage();
    var isDevelopment = mode.development();
    if(isProduction) {
      console.log("PRODUCTION mode");
    };
    if(isStage) {
      console.log("STAGE mode");
    };
    if(isDevelopment) {
      console.log("DEV mode");
    };

    // Browser Sync Init
    /*browserSync.init({
        server: {
            baseDir: dir.dist
        }
    });*/

    // Watch Styles
    gulp.watch(path.styles.src + '**/*', gulp.series('css'));
    gulp.watch(path.styles.dest, gulp.series('remote-deploy-css'));

    // Watch Scripts
    gulp.watch(path.scripts.src + '**/*', gulp.series('js'));
    gulp.watch(path.scripts.dest, gulp.series('remote-deploy-js'));

    //Watch Images
    gulp.watch(path.images.src + '**/*', gulp.series('imgmin'));
    gulp.watch(path.images.dest, gulp.series('remote-deploy-images'));

    // Browser Sync Reload
    //gulp.watch([dir.dist + '*.html', dir.dist + '**/*.css', dir.dist + '**/*.js', dir.dist + 'img/**/*']).on('change', browserSync.reload);
});

// Compile CSS
gulp.task('css', function(){
    const atimport = require("postcss-import");
    const postcss = require("gulp-postcss");
    //const purgecss = require("gulp-purgecss");

    return gulp
        .src(path.styles.src + '*.scss')
        .pipe( mode.development (
            sourcemaps.init()
            )
        )
        .pipe(sass().on('error', sass.logError))
        .pipe(
            mode.development (
                postcss([
                    atimport(),
                    autoprefixer()
                ])
            )
        )
        .pipe(
            mode.stage(
                postcss([
                    atimport(),
                    autoprefixer()
                ])
            )
        )
        .pipe(
            mode.production(
                postcss([
                    atimport(),
                    autoprefixer(),
                    cssnano()
                ])
            )
        )
        .pipe( mode.development(
            sourcemaps.write()
        ))
        .pipe(gulp.dest(path.styles.dest))
        //.pipe(browserSync.stream());
});

// Compile JS
gulp.task('js', function(){
    return gulp.src(path.scripts.src + 'script-logic.js')
        .pipe(jsImport({hideConsole: true}))
        .pipe(concat('scripts.bundle.js'))
        .pipe(mode.production(uglify()))
        .pipe(gulp.dest(path.scripts.dest));
});

// Minify Images
gulp.task("imgmin", imgMinify);

// Google Fonts
gulp.task('getFonts', function () {
  return gulp.src(path.src + 'gfonts.neon')
    .pipe(googlefonts())
    .pipe(gulp.dest(path.styles.src + 'fonts'));
});

// Copy resources
gulp.task('copy-resources', function() {
    return merge([
        gulp.src('node_modules/bootstrap-icons/font/**/*.*')
            .pipe(gulp.dest(path.styles.dest + '/bootstrap-icons'))
      //gulp.src('./src/data/*.json').pipe(gulp.dest('./deploy/data'))
  ]);
});

// Imgmin
function imgMinify() {
    return gulp.src(path.images.src + "*.*")
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(path.images.dest));
};

// Deploy to remote server
var localCss = [
    path.dest + 'css**/*',
    '!*.git',
    '!*.md',
    '!*.json'
];
var localJs = [
    path.dest + 'js**/*',
    '!*.git',
    '!*.md',
    '!*.json'
];
var localImages = [
    path.dest + 'assets/img**/*',
    '!*.git',
    '!*.md',
    '!*.json'
];
var remoteLocation = path.remote.dest;

function getFtpConnection(){
    return ftp.create({
        host: path.remote.host,
        port: 21,
        user: path.remote.user,
        password: path.remote.pass,
        parallel: 5,
        log: gutil.log
    })
}

gulp.task('remote-deploy-css',function(){
    var conn = getFtpConnection();
    return gulp.src(localCss, {base: path.dest, buffer: false})
        .pipe(conn.newer(remoteLocation))
        .pipe(conn.dest(remoteLocation))
});

gulp.task('remote-deploy-js',function(){
    var conn = getFtpConnection();
    return gulp.src(localJs, {base: path.dest, buffer: false})
        .pipe(conn.newer(remoteLocation))
        .pipe(conn.dest(remoteLocation))
});

gulp.task('remote-deploy-images',function(){
    var conn = getFtpConnection();
    return gulp.src(localImages, {base: path.dest, buffer: false})
        .pipe(conn.newer(remoteLocation))
        .pipe(conn.dest(remoteLocation))
});

// DEFAULT task
gulp.task('default', gulp.series('css', 'js', 'watch'));
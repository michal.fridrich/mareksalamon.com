( function () {
    'use strict';
    // Focus input if Searchform is empty
    [].forEach.call( document.querySelectorAll( '.search-form' ), ( el ) => {
        el.addEventListener( 'submit', function ( e ) {
            var search = el.querySelector( 'input' );
            if ( search.value.length < 1 ) {
                e.preventDefault();
                search.focus();
            }
        } );
    } );

    // Initialize Popovers: https://getbootstrap.com/docs/5.0/components/popovers
    var popoverTriggerList = [].slice.call( document.querySelectorAll( '[data-bs-toggle="popover"]' ) );
    var popoverList = popoverTriggerList.map( function ( popoverTriggerEl ) {
        return new bootstrap.Popover( popoverTriggerEl, {
            trigger: 'focus',
        } );
    } );
} )();

/* Load Youtube on List Item Click */

jQuery(document).on('click', '.works-youtube', function(event) {
    //var text = jQuery(this).text();
    var videoKey = jQuery(this).data('video');
    var videoSlug = jQuery(this).data('slug');
    var videoPlayer = "#videoplayer-" + videoSlug
    jQuery("#videoplayer-" + videoSlug).attr('src', videoKey);
});

/* Unload Youtube on List Item Click */

jQuery(document).on('click', '.offcanvas-close', function(event) {
    var videoPlaying = jQuery(this).data('videoplaying');
    jQuery("#videoplayer-" + videoPlaying).attr('src', '');
});

jQuery(document).on('click', '.works-item-next, .works-item-prev', function(event) {
    //var text = jQuery(this).text();
    var videoKey = jQuery(this).data('video');
    var videoSlug = jQuery(this).data('slug');
    var videoPlaying = jQuery(this).data('videoplaying');
    var videpPlayer = "#videoplayer-" + videoSlug
    jQuery("#videoplayer-" + videoPlaying).attr('src', '');
    jQuery("#videoplayer-" + videoSlug).attr('src', videoKey);
});

jQuery(document).on('mouseover mouseleave', '.works-item', function (event) {
    const mediaQueryHero = window.matchMedia('(min-width: 768px)')
    if (event.type === "mouseover") {
        jQuery(".works-item").css("opacity", ".5").css("box-shadow", "0px 0px 50px black" );
        jQuery(this).css("opacity", "1");
    } else if (event.type === "mouseleave") {
        jQuery(".works-item").css("opacity", "1").css("box-shadow", "0px 0px 0px black" );
    }
    // Check if the media query is true
    if (mediaQueryHero.matches) {
        var thumbnailImageUrl = jQuery(this).attr("data-thumbnail");
        var thumbnailImageUrl = "url(" + thumbnailImageUrl + ")";
        //console.log(thumbnailImageUrl);
        if (event.type === "mouseover") {
            jQuery(".hero-background--home").css("background-image", thumbnailImageUrl);
            jQuery(".hero-background--home").css("opacity", "0.2");
        } else if (event.type === "mouseleave") {
            jQuery(".hero-background--home").css("background-image", "none");
            jQuery(".hero-background--home").css("opacity", "0");
        }
    }
});

/* Header + scroll padding */
document.addEventListener("DOMContentLoaded", function(event) {
    let navbarHeaderH = document.getElementById("header").offsetHeight;
    let navbarHeaderHc = document.getElementById("header").clientHeight;
    jQuery("html").css({"scroll-padding-top": navbarHeaderH + "px"});
});

/* Hero - Homepage */
function setHeroHeight() {
    let windowHeight = window.innerHeight;
    //console.log(windowHeight);
    jQuery('#hero-home').css({"height": windowHeight + "px"});
    jQuery('#hero-background').css({"height": windowHeight + "px"});
}
window.addEventListener('resize', setHeroHeight);
setHeroHeight();

/* Navbar Collapse */
var navbarCollapse = document.getElementById('navbar-primary')
navbarCollapse.addEventListener('show.bs.collapse', function () {
   jQuery("#header").addClass("navbar-top-open");
});
navbarCollapse.addEventListener('hidden.bs.collapse', function () {
   jQuery("#header").removeClass("navbar-top-open");
});

jQuery('.nav-link').click(function(){
    jQuery('#navbar-primary').removeClass('show');
    jQuery("#header").removeClass("navbar-top-open");
});
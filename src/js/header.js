if (window.matchMedia("(min-width: 1px)").matches) {
    jQuery(function($) {
        // Hide Header on on scroll down
        var didScroll;
        var lastScrollTop = 0;
        var delta = 5;
        var navbarHeight = $('#header').outerHeight();

        $(window).scroll(function(event){
            didScroll = true;
        });

        setInterval(function() {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);

        function hasScrolled() {
            var st = $(this).scrollTop();

            // Make sure they scroll more than delta
            if(Math.abs(lastScrollTop - st) <= delta)
                return;

            // If they scrolled down and are past the navbar, add class .nav-up.
            // This is necessary so you never see what is "behind" the navbar.
            if (st > lastScrollTop && st > navbarHeight){
                // Scroll Down
                $('#header').removeClass('show').addClass('hidden');
            } else {
                // Scroll Up
                if(st + $(window).height() < $(document).height()) {
                    $('#header').removeClass('hidden').addClass('show');
                }
            }

            lastScrollTop = st;
        }
    });
};

/*const mediaQueryMenuFilterShow = window.matchMedia('(max-width: 767px)')
// Check if the media query is true
if (mediaQueryMenuFilterShow.matches) {
    var menuFilter = document.getElementById('portfolio-categories'),
        clone = menuFilter.cloneNode(true); // true means clone all childNodes and all event handlers
    clone.id = "portfolio-categories--mobile";
    document.getElementById('navbar-secondary').appendChild(clone);
    document.getElementById('portfolio-categories--title').remove();
};*/

/*const mediaQueryMenuFilterHide = window.matchMedia('(min-width: 768px)')
// Check if the media query is true
if (mediaQueryMenuFilterHide.matches) {
    document.getElementById("header-secondary").remove();
};*/

/*jQuery(window).load(function(){

// Vieport height, fix later
    var vph = window.innerHeight;
    jQuery("html,body").scrollTop(vph).delay(2000);
});*/